<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class RoleDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'super-admin',
                'display_name' => 'Super Admin',
                'group' => 'System'
            ],
            [
                'name' => 'admin',
                'display_name' => 'Admin',
                'group' => 'System'
            ],
            [
                'name' => 'employee',
                'display_name' => 'Employee',
                'group' => 'System'
            ],
            [
                'name' => 'manager',
                'display_name' => 'Manager',
                'group' => 'System'
            ],
            [
                'name' => 'user',
                'display_name' => 'User',
                'group' => 'System'
            ],
        ];


        foreach ($roles as $role) {
            Role::updateOrCreate($role);
        }

        $permission = [
            [
                'name' => 'create-user',
                'display_name' => 'Create user',
                'group' => 'user'
            ],
            [
                'name' => 'update-user',
                'display_name' => 'update user',
                'group' => 'user'
            ],
            [
                'name' => 'delete-user',
                'display_name' => 'Delete user',
                'group' => 'user'
            ],
            [
                'name' => 'show-user',
                'display_name' => 'Show user',
                'group' => 'user'
            ],
            [
                'name' => 'create-role',
                'display_name' => 'Create role',
                'group' => 'role'
            ],
            [
                'name' => 'update-role',
                'display_name' => 'update role',
                'group' => 'role'
            ],
            [
                'name' => 'delete-role',
                'display_name' => 'Delete role',
                'group' => 'role'
            ],
            [
                'name' => 'show-role',
                'display_name' => 'Show role',
                'group' => 'role'
            ],
            [
                'name' => 'create-category',
                'display_name' => 'Create category',
                'group' => 'category'
            ],
            [
                'name' => 'update-category',
                'display_name' => 'update category',
                'group' => 'category'
            ],
            [
                'name' => 'delete-category',
                'display_name' => 'Delete category',
                'group' => 'category'
            ],
            [
                'name' => 'show-category',
                'display_name' => 'Show category',
                'group' => 'category'
            ],
            [
                'name' => 'create-product',
                'display_name' => 'Create product',
                'group' => 'product'
            ],
            [
                'name' => 'update-product',
                'display_name' => 'update product',
                'group' => 'product'
            ],
            [
                'name' => 'delete-product',
                'display_name' => 'Delete product',
                'group' => 'product'
            ],
            [
                'name' => 'show-product',
                'display_name' => 'Show product',
                'group' => 'product'
            ],
            [
                'name' => 'create-coupon',
                'display_name' => 'Create coupon',
                'group' => 'coupon'
            ],
            [
                'name' => 'update-coupon',
                'display_name' => 'update coupon',
                'group' => 'coupon'
            ],
            [
                'name' => 'delete-coupon',
                'display_name' => 'Delete coupon',
                'group' => 'coupon'
            ],
            [
                'name' => 'show-coupon',
                'display_name' => 'Show coupon',
                'group' => 'coupon'
            ]
        ];

        foreach ($permission as $item) {
            Permission::updateOrCreate($item);
        }
    }
}
