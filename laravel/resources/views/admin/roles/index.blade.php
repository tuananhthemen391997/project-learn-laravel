@extends('admin.layouts.app')

@section('title', 'Roles')

@section('content')
    @if(session('message'))
        <span>{{ session('message') }}</span>
    @endif

    <div class="card justify-content-center">
        Role list
    </div>
    <div>
        <a href="{{route('roles.create')}}" class="btn btn-primary">Create role</a>
    </div>
    <div>
        <table class="table table-hover">
            <tr>
                <td>#</td>
                <td>Name</td>
                <td>Display name</td>
                <td>Action</td>
            </tr>
            @foreach($roles as $role)
                <tr>
                    <td>{{ $role->id }}</td>
                    <td>{{ $role->name }}</td>
                    <td>{{ $role->display_name }}</td>
                    <td>
                        <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-warning">Edit</a>
                        <a href="{{ route('roles.destroy', $role->id) }}" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $roles->links('pagination::bootstrap-4') }}
    </div>
@endsection
